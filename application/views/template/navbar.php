<nav class="navbar navbar-expand-lg sticky-top navbar-dark" style="background-color: rgba(0, 0, 0, 0.3)">
    <div class="container">
        <a class="navbar-brand" href="#">
            <img src="<?= base_url() ?>assets/img/bujangGadis.png" height="35" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link"href="<?= base_url() ?>" style="color: gold">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <button class="nav-link btn-link btn" id="pp" style="color: gold; margin-top: -1px;">Persyaratan <span class="sr-only">(current)</span></button>
                </li>
                <li class="nav-item active">
                    <button class="nav-link btn-link btn" id="bb" style="color: gold; margin-top: -1px;">Berkas <span class="sr-only">(current)</span></button>
                </li>
                <li class="nav-item active">
                    <button class="nav-link btn-link btn" id="jj" style="color: gold; margin-top: -1px;">Jadwal <span class="sr-only">(current)</span></button>
                </li>
                <?php if($this->session->userdata('username') == '') { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="<?= site_url('login') ?>" style="color: gold">Login</a>
                </li>
                <?php } else { ?>
                <li class="nav-item active">
                    <a class="nav-link" href="<?= site_url('logout') ?>" style="color: gold">Logout</a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>