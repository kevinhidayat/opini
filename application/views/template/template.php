<?php
    $this->load->view('template/title', $title);
    if($content != 'nomor' &&    $content !='soal'){
        $this->load->view('template/navbar');
    }
    $this->load->view($content);
    $this->load->view('template/footer');
?>