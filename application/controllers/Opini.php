<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Opini extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Opini_m');
    }

    public function index()
    {
        $this->data['title'] = 'Opini | ';
        $this->data['content'] = 'nomor';
        $this->data['soal'] = $this->Opini_m->get();
        $this->load->view('template/template',$this->data);
    }

    public function soal($id)
    {
        $this->data['title'] = 'Soal | ';
        $this->data['content'] = 'soal';
        $this->data['soal'] = $this->Opini_m->get_row(array('id' => $id));
        $this->load->view('template/template', $this->data);
    }
}

/* End of file Controllername.php */
